//
//  RecipeModel.swift
//  recipe app
//
//  Created by Mohamad Kamal Zakaria on 19/08/2020.
//  Copyright © 2020 Mohamad Kamal Zakaria. All rights reserved.
//

import SwiftUI

// MARK: - RECIPE MODEL

struct Recipe: Identifiable {
    var id = UUID()
    var title: String
    var headline: String
    var image: String
    var type: String
    var instructions: [String]
    var ingredients: [String]
}
