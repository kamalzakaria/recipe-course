//
//  RecipeCardView.swift
//  recipe app
//
//  Created by Mohamad Kamal Zakaria on 19/08/2020.
//  Copyright © 2020 Mohamad Kamal Zakaria. All rights reserved.
//

import SwiftUI
import URLImage

struct RecipeCardView: View {
    // MARK: - PROPERTIES
    
    var recipe: Recipe
    var resepi: Resepi?
    var recipeTypes: [String]
    var hapticImpact = UIImpactFeedbackGenerator(style: .heavy)
    
    @State private var showModal: Bool = false
    
    @Environment(\.managedObjectContext) var managedObjectContext
    
    init(recipe: Recipe?, resepi: Resepi?, recipeTypes: [String]) {
        self.resepi = resepi
        self.recipeTypes = recipeTypes
        if (recipe == nil && resepi != nil) {
            let thisRecipe = Recipe(title: resepi?.title ?? "", headline: resepi?.headline ?? "", image: resepi?.image ?? "", type: resepi?.type ?? "", instructions: resepi?.instructions as? [String] ?? [], ingredients: resepi?.ingredients as? [String] ?? [])
            self.recipe = thisRecipe
        } else {
            self.recipe = recipe ?? recipeData[0]
        }
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            // CARD IMAGE
            if (resepi != nil) {
                URLImage(URL(
                    string: recipe.image)!,
                         content:  {
                            $0.image
                                .resizable()
                                .scaledToFit()
                })
            } else {
                Image(recipe.image)
                    .resizable()
                    .scaledToFit()
            }
            
            VStack(alignment: .leading, spacing: 12) {
                // TITLE
                Text(recipe.title)
                    .font(.system(.title, design: .serif))
                    .fontWeight(.bold)
                    .foregroundColor(Color("ColorGreenMedium"))
                    .lineLimit(1)
                
                // HEADLINE
                Text(recipe.headline)
                    .font(.system(.body, design: .serif))
                    .foregroundColor(.gray)
                    .italic()
            }
            .padding()
            .padding(.bottom, 12)
            
        }
        .background(Color(.white))
        .cornerRadius(12)
        .shadow(color: Color("ColorBlackTransparentLight"), radius: 8, x: 0, y: 0)
        .onTapGesture {
            self.hapticImpact.impactOccurred()
            self.showModal = true
        }
        .sheet(isPresented: self.$showModal, content: {
            RecipeDetailView(recipe: self.recipe, resepi: self.resepi, recipeTypes: self.recipeTypes)
                .environment(\.managedObjectContext, self.managedObjectContext)
        })
    }
}

struct RecipeCardView_Previews: PreviewProvider {
    static var previews: some View {
        RecipeCardView(recipe: recipeData[0], resepi: nil, recipeTypes: ["Veggies, Non-Veggies"])
            .previewLayout(.sizeThatFits)
    }
}
