//
//  RecipeEditView.swift
//  recipe app
//
//  Created by Mohamad Kamal Zakaria on 20/08/2020.
//  Copyright © 2020 Mohamad Kamal Zakaria. All rights reserved.
//

import SwiftUI
import Combine

struct RecipeEditView: View {
    // MARK: - PROPERTIES
    
    var resepi: Resepi?
    
    @Environment(\.managedObjectContext) var managedObjectContext
    @Environment(\.presentationMode) var presentationMode
    
    @State private var name: String = ""
    @State private var imageUrl: String = ""
    @State private var description: String = ""
    @State private var instructions: String = ""
    @State private var ingredients: String = ""
    @State private var type: String = "Veggies"
    
    var types: [String] = ["Veggies", "Non-Veggies"]
    
    @State private var errorShowing: Bool = false
    @State private var errorTitle: String = ""
    @State private var errorMessage: String = ""
    
    init(resepi: Resepi?, types: [String]) {
        self.resepi = resepi
    }
    
    // MARK: - BODY
    var body: some View {
        NavigationView {
            ScrollView {
                VStack {
                    VStack(alignment: .leading, spacing: 20) {
                        
                        Text("Recipe Details")
                            .font(.system(.body, design: .serif))
                            .fontWeight(.bold)
                            .foregroundColor(Color("ColorGreenAdaptive"))
                        // MARK: - RECIPE IMAGE URL
                        TextField("Image Url", text: $imageUrl)
                            .modifier(TextFieldModifier())
                        
                        // MARK: - RECIPE IMAGE URL
                        TextField("Name", text: $name)
                            .modifier(TextFieldModifier())
                        
                        // MARK: - RECIPE IMAGE URL
                        TextField("Description", text: $description)
                            .modifier(TextFieldModifier())
                        
                        // MARK: - RECIPE IMAGE URL
                        Text("Ingredients (separate with new line)")
                            .font(.system(.body, design: .serif))
                            .fontWeight(.bold)
                            .foregroundColor(Color("ColorGreenAdaptive"))
                        MultilineTextField(text: $ingredients)
                        
                        
                        // MARK: - RECIPE IMAGE URL
                        Text("Instructions (separate with new line)")
                            .font(.system(.body, design: .serif))
                            .fontWeight(.bold)
                            .foregroundColor(Color("ColorGreenAdaptive"))
                        MultilineTextField(text: $instructions)
                        // MARK: - TODO PRIORITY
                        Picker("Type", selection: $type) {
                            ForEach(types, id: \.self) {
                                Text($0)
                            }
                        }
                        .pickerStyle(SegmentedPickerStyle())
                        
                        // MARK: - SAVE BUTTON
                        Button(action: {
                            if self.name != "" {
                                let instructions = self.instructions.split(whereSeparator: \.isNewline)
                                let ingredients = self.ingredients.split(whereSeparator: \.isNewline)
                                let recipe = self.resepi ?? Resepi(context: self.managedObjectContext)
                                recipe.title = self.name
                                recipe.headline = self.description
                                recipe.image = self.imageUrl
                                recipe.type = self.type
                                recipe.instructions = instructions as NSObject
                                recipe.ingredients = ingredients as NSObject
                                do {
                                    try self.managedObjectContext.save()
                                } catch {
                                    print(error)
                                }
                            } else {
                                self.errorShowing = true
                                self.errorTitle = "Invalid Name"
                                self.errorMessage = "Make sure to enter something for\nthe new todo item."
                                return
                            }
                            self.presentationMode.wrappedValue.dismiss()
                        }) {
                            Text("Save")
                                .font(.system(size: 24, weight: .bold, design: .default))
                                .padding()
                                .frame(minWidth: 0, maxWidth: .infinity)
                                .background(Color("ColorGreenAdaptive"))
                                .cornerRadius(9)
                                .foregroundColor(Color.white)
                        } //: SAVE BUTTON
                    } //: VSTACK
                        .padding(.horizontal)
                        .padding(.vertical, 30)
                    
                    Spacer()
                } //: VSTACK
                    .navigationBarTitle("New Recipe", displayMode: .inline)
                    .navigationBarItems(trailing:
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }) {
                            Image(systemName: "xmark")
                        }
                )
                    .alert(isPresented: $errorShowing) {
                        Alert(title: Text(errorTitle), message: Text(errorMessage), dismissButton: .default(Text("OK")))
                }
            } //: SCROLLVIEW
                .modifier(AdaptsToKeyboard())
                .onAppear {
                    self.name = self.resepi?.title ?? ""
                    self.imageUrl = self.resepi?.image ?? ""
                    self.description = self.resepi?.headline ?? ""
                    self.type = self.resepi?.type ?? ""
                    self.instructions = (self.resepi?.instructions as? [String] ?? []).joined(separator: "\n")
                    self.ingredients = (self.resepi?.ingredients as? [String] ?? []).joined(separator: "\n")
                    
            }
        } //: NAVIGATION
            .accentColor(Color("ColorGreenAdaptive"))
            .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct TextFieldModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .padding()
            .background(Color(UIColor.tertiarySystemFill))
            .cornerRadius(9)
            .font(.system(size: 18, weight: .medium, design: .default))
    }
}

struct AdaptsToKeyboard: ViewModifier {
    @State var currentHeight: CGFloat = 0
    
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
                .padding(.bottom, self.currentHeight)
                .animation(.easeOut(duration: 0.16))
                .onAppear(perform: {
                    NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillShowNotification)
                        .merge(with: NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillChangeFrameNotification))
                        .compactMap { notification in
                            notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as? CGRect
                    }
                    .map { rect in
                        rect.height - geometry.safeAreaInsets.bottom
                    }
                    .subscribe(Subscribers.Assign(object: self, keyPath: \.currentHeight))
                    
                    NotificationCenter.Publisher(center: NotificationCenter.default, name: UIResponder.keyboardWillHideNotification)
                        .compactMap { notification in
                            CGFloat.zero
                    }
                    .subscribe(Subscribers.Assign(object: self, keyPath: \.currentHeight))
                })
        }
    }
}

struct RecipeEditView_Previews: PreviewProvider {
    static var previews: some View {
        RecipeEditView(resepi: nil, types: ["Veggies", "Non-Veggies"])
            .previewDevice("iPhone 11 Pro")
    }
}
