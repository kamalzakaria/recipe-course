//
//  RecipeDetailView.swift
//  recipe app
//
//  Created by Mohamad Kamal Zakaria on 20/08/2020.
//  Copyright © 2020 Mohamad Kamal Zakaria. All rights reserved.
//

import SwiftUI
import URLImage

struct RecipeDetailView: View {
    // MARK: - PROPERTIES
    
    var recipe: Recipe
    var resepi: Resepi?
    var recipeTypes: [String]
    
    @State private var pullsate: Bool = false
    @State private var showingAddRecipeView: Bool = false

    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) var managedObjectContext
    
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .center, spacing: 0) {
                if (resepi != nil) {
                    URLImage(URL(
                        string: recipe.image)!,
                             content:  {
                                $0.image
                                    .resizable()
                                    .scaledToFit()
                    })
                } else {
                    Image(recipe.image)
                        .resizable()
                        .scaledToFit()
                }
                Group {
                    // MARK: - TITLE
                    Text(recipe.title)
                        .font(.system(.largeTitle, design: .serif))
                        .fontWeight(.bold)
                        .multilineTextAlignment(.center)
                        .foregroundColor(Color("ColorGreenAdaptive"))
                        .padding(.top, 10)
                    
                    // MARK: - HEADLINE
                    Text(recipe.headline)
                        .font(.system(.body, design: .serif))
                        .foregroundColor(.gray)
                        .italic()
                    
                    // MARK: - INGREDIENTS
                    Text("Ingredients")
                        .fontWeight(.bold)
                        .modifier(TitleModifier())
                    
                    VStack(alignment: .leading, spacing: 5) {
                        ForEach(recipe.ingredients, id: \.self) { item in
                            VStack(alignment: .leading, spacing: 5) {
                                Text(item)
                                    .font(.footnote)
                                    .multilineTextAlignment(.leading)
                                Divider()
                            }
                        }
                    }
                    
                    // MARK: - INSTRUCTIONS
                    Text("Instructions")
                        .fontWeight(.bold)
                        .modifier(TitleModifier())
                    
                    ForEach(recipe.instructions, id: \.self) { item in
                        VStack(alignment: .center, spacing: 5) {
                            Image(systemName: "chevron.down.circle")
                                .resizable()
                                .frame(width: 42, height: 42, alignment: .center)
                                .imageScale(.large)
                                .font(Font.title.weight(.ultraLight))
                                .foregroundColor(Color("ColorGreenAdaptive"))
                            Text(item)
                                .lineLimit(nil)
                                .multilineTextAlignment(.center)
                                .font(.system(.body, design: .serif))
                                .frame(minHeight: 100)
                        }
                    }
                    
                    Divider()
                    
                    // MARK: - SAVE BUTTON
                    if (self.resepi != nil) {
                        HStack {
                            Button(action: {
                                // ACTION
                                self.deleteResepi(resepi: self.resepi)
                                self.presentationMode.wrappedValue.dismiss()
                            }) {
                                Text("Delete")
                                    .font(.system(size: 20, weight: .bold, design: .default))
                                    .padding()
                                    .frame(minWidth: 0)
                                    .background(Color(.systemRed))
                                    .cornerRadius(9)
                                    .foregroundColor(Color.white)
                            }
                            Button(action: {
                                // ACTION
                                self.showingAddRecipeView.toggle()
                            }) {
                                Text("Edit")
                                    .font(.system(size: 20, weight: .bold, design: .default))
                                    .padding()
                                    .frame(minWidth: 0)
                                    .background(Color("ColorGreenAdaptive"))
                                    .cornerRadius(9)
                                    .foregroundColor(Color.white)
                            }
                            Spacer()
                        } //: SAVE BUTTON
                    }
                }
                .padding(.horizontal, 24)
                .padding(.vertical, 12)
            }
        }
        .overlay(
            HStack {
                Spacer()
                VStack {
                    Button(action: {
                        // ACTION
                        self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Image(systemName: "chevron.down.circle.fill")
                            .font(.title)
                            .foregroundColor(.white)
                            .shadow(radius: 4)
                            .opacity(self.pullsate ? 1 : 0.6)
                            .scaleEffect(self.pullsate ? 1.2 : 0.8, anchor: .center)
                            .animation(Animation.easeInOut(duration: 1.5).repeatForever(autoreverses: true))
                        
                    })
                        .padding(.trailing, 20)
                        .padding(.top, 24)
                    Spacer()
                }
            }
        )
        .sheet(isPresented: $showingAddRecipeView) {
            RecipeEditView(resepi: self.resepi, types: self.recipeTypes)
                .environment(\.managedObjectContext, self.managedObjectContext)
        }
            .onAppear() {
                self.pullsate.toggle()
        }
    }
    
    // MARK: - FUNCTIONS
    
    private func deleteResepi(resepi: Resepi?) {
        if let recipe = resepi {
            managedObjectContext.delete(recipe)
            do {
                try managedObjectContext.save()
            } catch {
                print(error)
            }
        }
    }
}

struct RecipeDetailView_Previews: PreviewProvider {
    static var previews: some View {
        RecipeDetailView(recipe: recipeData[0], recipeTypes: ["Veggies", "Non-Veggies"])
    }
}
