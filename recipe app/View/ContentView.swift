//
//  ContentView.swift
//  recipe app
//
//  Created by Mohamad Kamal Zakaria on 19/08/2020.
//  Copyright © 2020 Mohamad Kamal Zakaria. All rights reserved.
//

import SwiftUI
import SwiftyXMLParser

struct ContentView: View {
    // MARK: - PROPERTIES
    
    var recipes: [Recipe] = recipeData
    var filters: [String] = ["All"]
    var recipeTypes: [String] = []
    var hapticImpact = UIImpactFeedbackGenerator(style: .heavy)
    
    @State private var filteredRecipes: [Recipe] = recipeData
    @State private var pickerIndex: Int = 0
    @State private var animatingButton: Bool = false
    @State private var showingAddRecipeView: Bool = false
    
    @Environment(\.managedObjectContext) var managedObjectContext
    
    @FetchRequest(entity: Resepi.entity(), sortDescriptors: [NSSortDescriptor(keyPath: \Resepi.title, ascending: true)]) var resepis: FetchedResults<Resepi>
    
    
    init() {
        if let xmlPath = Bundle.main.path(forResource: "recipetypes", ofType: "xml") {
            if let xmlData = NSData(contentsOfFile: xmlPath) {
                let xml = XML.parse(xmlData as Data)
                for element in xml.RecipeTypes.Type {
                    if let type = element.text {
                        filters.append(type)
                        recipeTypes.append(type)
                    }
                }
            }
        }
    }
    
    var body: some View {
        
        let pi = Binding<Int>(get: {
            return self.pickerIndex
        }, set: {
            self.pickerIndex = $0
            if ($0 == 0) {
                self.filteredRecipes = self.recipes
            } else {
                self.filteredRecipes = self.recipes.filter({ $0.type == self.filters[self.pickerIndex]})
            }
        })
        
        return VStack(alignment: .center, spacing: 5) {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 20) {
                    // MARK: - RECIPE CARD
                    Text("Recipes")
                        .fontWeight(.bold)
                        .modifier(TitleModifier())
                    
                    VStack(alignment: .center, spacing: 20) {
                        ForEach(resepis.filter({ self.pickerIndex == 0 ? true : $0.type == self.filters[self.pickerIndex]}), id: \.self) { item in
                            RecipeCardView(recipe: nil, resepi: item, recipeTypes: self.recipeTypes)
                        } //: FOREACH
                        ForEach(filteredRecipes) { item in
                            RecipeCardView(recipe: item, resepi: nil, recipeTypes: self.recipeTypes)
                                .environment(\.managedObjectContext, self.managedObjectContext)
                        } //: FOREACH
                    } //: VSTACK
                        .frame(maxWidth: 640)
                        .padding(.horizontal)
                        .padding(.bottom, 85)
                    
                } //: VSTACK
            } //: SCROLLVIEW
            HStack(alignment: .center, spacing: 5) {
                
                Spacer()
                
                // MARK: - RECIPE TYPE PICKER VIEW
                Picker(selection: pi, label: Text("Select Type")) {
                    ForEach(0 ..< filters.count) {
                        Text(self.filters[$0])
                            .font(.system(.body, design: .serif))
                            .fontWeight(.bold)
                            .foregroundColor(Color("ColorGreenAdaptive"))
                    }
                } //: PICKER
                    .frame(width: 200, height: 100)
                    .clipped()
                
                Spacer()
                
                // MARK: - ADD BUTTON
                ZStack {
                    Group {
                        Circle()
                            .fill(Color("ColorGreenAdaptive"))
                            .opacity(self.animatingButton ? 0.2 : 0)
                            .scaleEffect(self.animatingButton ? 1 : 0)
                            .frame(width: 68, height: 68, alignment: .center)
                        Circle()
                            .fill(Color("ColorGreenAdaptive"))
                            .opacity(self.animatingButton ? 0.15 : 0)
                            .scaleEffect(self.animatingButton ? 1 : 0)
                            .frame(width: 88, height: 88, alignment: .center)
                    } //: GROUP
                        .animation(Animation.easeInOut(duration: 2).repeatForever(autoreverses: true))
                    
                    Button(action: {
                        self.showingAddRecipeView.toggle()
                    }) {
                        Image(systemName: "plus.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .background(Circle().fill(Color("ColorAppearanceAdaptive")))
                            .frame(width: 48, height: 48, alignment: .center)
                    } //: BUTTON
                        .accentColor(Color("ColorGreenAdaptive"))
                        .onAppear(perform: {
                            self.animatingButton.toggle()
                        })
                } //: ZSTACK
            } //: HSTACK
        } //: VSTACK
            .sheet(isPresented: $showingAddRecipeView) {
                RecipeEditView(resepi: nil, types: self.recipeTypes)
                    .environment(\.managedObjectContext, self.managedObjectContext)
        }
    }
}


struct TitleModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.system(.title, design: .serif))
            .foregroundColor(Color("ColorGreenAdaptive"))
            .padding(8)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
