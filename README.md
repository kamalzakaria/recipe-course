# SwiftUI Recipe Apps

<img src="screenshots/home_light.png" width="200"> <img src="screenshots/home_dark.png" width="200"> <img src="screenshots/recipe_details_dark.png" width="200"> <img src="screenshots/new_recipe_dark.png" width="200">

This is written based on tutorial by [SwiftUI Masterclass](https://www.udemy.com/course/swiftui-masterclass-course-ios-development-with-swift/)

# Criteria
- Use Swift (preferred) or Objective-C as the programming language
- Create an xml file with recipe types data (recipetypes.xml), use that to populate the recipe types into a UIPickerView control
- Create a listing page to list out all recipes (filterable by recipe types from recipetypes.xml)
- Pre-populate your own sample recipes data complying with recipetypes.xml
- Create an Add Recipe page based on available recipe type with picture, ingredients and steps and update the existing list
- Create a Recipe Detail page that display the recipe’s image along with the ingredients and steps. This page should include update (all displaying items should be editable) and delete feature
- Use at least one type of persistence method available in iOS to store data
- Upload the project into any public Git hosting services and ensure that your project is buildable

# Features
- Some cool animations
- Support for dark mode
- Using Core Data for persistence storage
- Using Pod for dependencies ([SwiftyXMLParser](https://github.com/yahoojapan/SwiftyXMLParser) & [URLImage](https://github.com/dmytro-anokhin/url-image))

